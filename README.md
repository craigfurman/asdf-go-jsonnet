# asdf-go-jsonnet

**This repository is no longer maintained**. Please do not open issues or merge
requests, except to suggest a fork for this repository to point folks towards.

As [asdf](https://github.com/asdf-vm/asdf) plugin for
[go-jsonnet](https://github.com/google/go-jsonnet).

## Installation

```
asdf plugin add go-jsonnet
```

This plugin installs binaries named `jsonnet` and `jsonnetfmt`, so it will
conflict in your `$PATH` with any other jsonnet installation.

Requires:

* go
* curl
* jq
